import React from 'react';
import Page from '../../app/hoc/securedPage';
import asyncComponent from '../../util/asyncComponent'

const SamplePage = asyncComponent(() => import('../../routes/Sample'));

export default Page(() => <SamplePage/>);
